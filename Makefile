php_version=5.6.2
php_zip_basename=php-$(php_version)-Win32-VC11-x86
php_zip=$(php_zip_basename).zip
php_zip_url=http://installer-bin.streambox.com/$(php_zip)
curl_agent='Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.71 Safari/537.17'
RM=rm -f

test: composer.json
	php/php -S localhost:8000 t1.php

composer.json: composer.phar
composer.json: php
	sh -x gettwig.sh

composer.phar: php
	cp php/php.ini-development php/php.ini
	sh -x composer-install.sh
	-diff -uw php/php.ini-development php/php.ini

version: php
	sh -x getversion.sh

php: $(php_zip)
	7z x -o$@ $(php_zip) >/dev/null
	chmod -R +x php

$(php_zip):
	curl -A "$(curl_agent)" -O $(php_zip_url)

veryclean:
	$(RM) -f php-*-*-VC*-x*.zip

clean:
	$(RM) -r php
	$(RM) -r vendor
	$(RM) composer.lock
	$(RM) composer.json
