PATH=php:/usr/bin
PATH=$PATH:/usr/bin

sed -i.bak \
	-e 's/^;extension=php_openssl.dll/extension=php_openssl.dll/' \
	-e 's/^; extension_dir = "ext"/extension_dir = ext/' \
	php/php.ini

curl -s 'http://getcomposer.org/installer' | php -c php
